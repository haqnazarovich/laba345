﻿using ClassLibary_3;
using System;

namespace ТПРО3
{
    class Program
    {

        static void Main(string[] args)
        {
            SquareClass sq = new SquareClass();
            try
            {
                Console.WriteLine("Расчет площади сектора круга ");
                Console.WriteLine("Введите градусы угла сектора ");
                double Degree = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Введите радиус сектора");
                double Radius = Convert.ToDouble(Console.ReadLine());
                if (Degree < 0 || Degree > 360)
                {
                    Console.WriteLine("Угол не может быть меньше 0 и больше 360");
                    throw new Exception();
                }
                else
                {
                    double s = sq.GetSq(Degree, Radius);
                    Console.WriteLine($"Площадь сектора = {s} ед. изм.");
                }
            }
            catch
            {
                Main(args);
            }
            Console.ReadKey();

        }
    }

}
