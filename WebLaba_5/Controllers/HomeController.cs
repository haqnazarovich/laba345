﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.ComponentModel.DataAnnotations;
using ClassLibary_3;

namespace WebLaba_5
{
    public class HomeController : Controller
    {


        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]

        public ActionResult Index(string Degree, string Radius)
        {

            double Square = new SquareClass().GetSq(Convert.ToDouble(Degree), Convert.ToDouble(Radius));
            if (Square >= 0)
            {
                ViewBag.result = Square;
            }
            if (Convert.ToDouble(Degree) > 360 || Convert.ToDouble(Degree) < 0)
            {
                ViewBag.result = "Градус должен быть больше 0 и меньше 360";
            }
            if (Convert.ToDouble(Radius) < 0)
            {
                ViewBag.result = "Радиус не может быть отрицательным";
            }

            return View();
        }



    }
}
