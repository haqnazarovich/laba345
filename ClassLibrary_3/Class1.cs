﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace ClassLibary_3
{
    public class SquareClass : INotifyPropertyChanged
    {
        public SquareClass() { }

        public double GetSq(double sectorDeg, double SecR) => Math.Round(Math.PI * Math.Pow(SecR, 2) * (sectorDeg / 360), 2);

        private double _sectorDegree;
        public double SectorDegree
        {
            get { return _sectorDegree; }
            set
            {
                _sectorDegree = value;
                OnPropertyChanged("S");
            }
        }
        private double _sectorR;
        public double SectorR
        {
            get { return _sectorR; }
            set
            {
                _sectorR = value;
                OnPropertyChanged("S");
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public double S { get { return this.GetSq(SectorDegree, SectorR); } }

    }


}
